# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright 2018 Odoo IT now <http://www.odooitnow.com/>
# See LICENSE file for full copyright and licensing details.
#
##############################################################################

from odoo import api, models, fields, _
import csv
from io import StringIO
import base64
from datetime import datetime


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def export_orders(self, order=False):
        buffer = StringIO()
        writer = csv.writer(buffer, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Cliente: ', self.env.user.name])
        writer.writerow(['Fecha de Exportación: ', str(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
        writer.writerow([])
        if order:
            writer.writerow(['Cotización N°,
                             'Estado',
                             'Fecha Validez',
                             'Total'])
        else:
            writer.writerow(['Cotización N°,
                             'Estado',
                             'Fecha Validez',
                             'Total'])
        for rec in self.sudo():
            writer.writerow([rec.name or '',
                             rec.x_fase_cotizacion or '',
                             rec.validity_date or '',
                             rec.amount_total])
        out = base64.encodestring(('%s' % (buffer.getvalue())).encode()).decode().replace('\n', '')
        buffer.close()
        record = self.env['invoice.report.file'].create({'file': out})
        return  '/web/binary/download_document?model=invoice.report.file&field=file&id=%s&filename=cotizaciones_exportadas.csv' % (record.id)
