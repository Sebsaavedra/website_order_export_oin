# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright 2018 Odoo IT now <http://www.odooitnow.com/>
# See LICENSE file for full copyright and licensing details.
#
##############################################################################

from odoo import api, models, fields, _
import csv
from io import StringIO
import base64
from datetime import datetime


class RepairOrder(models.Model):
    _inherit = 'repair.order'

    @api.multi
    def export_orders_repair(self, order=False):
        buffer = StringIO()
        writer = csv.writer(buffer, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Cliente: ', self.env.user.name])
        writer.writerow(['Fecha de Exportación: ', str(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
        writer.writerow([])
        if order:
            writer.writerow(['Certificado N°,
                             'Equipo',
                             'Categoria',
                             'Serie',
                             'Fecha de Expiración'])

        for rec in self.sudo():
            writer.writerow([rec.name or '',
                             rec.product_id or '',
                             rec.x_categoria or '',
                             rec.lot_id or '',
                             rec.x_fecha_de_expiracion or ''])
        out = base64.encodestring(('%s' % (buffer.getvalue())).encode()).decode().replace('\n', '')
        buffer.close()
        record = self.env['invoice.report.file'].create({'file': out})
        return  '/web/binary/download_document?model=invoice.report.file&field=file&id=%s&filename=certificados_exportados.csv' % (record.id)
