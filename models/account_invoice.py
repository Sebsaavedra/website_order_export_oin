# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright 2018 Odoo IT now <http://www.odooitnow.com/>
# See LICENSE file for full copyright and licensing details.
#
##############################################################################

from odoo import api, models, fields, _
import csv
from io import StringIO
import base64
from datetime import datetime


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def export_invoice(self):
        buffer = StringIO()
        writer = csv.writer(buffer, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Cliente: ', self.env.user.name])
        writer.writerow(['Fecha de Exportación: ', str(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
        writer.writerow([])
        writer.writerow(['Factura N°    ',
                         'Fecha factura',
                         'Fecha de vencimiento Date',
                         'Doc Origen',
                         'Estado',
                         'Total',
                         'Total adeudado'])
        for inv in self.sudo():
            state = 'Waiting for Payment'
            if inv.state == 'paid':
                state = 'Paid'
            if inv.state == 'cancel':
                state = 'Cancelled'
            writer.writerow([inv.number or '',
                             inv.date_invoice,
                             inv.date_due,
                             inv.origin or '',
                             state,
                             inv.amount_total_signed,
                             inv.residual_signed])
        out = base64.encodestring(('%s' % (buffer.getvalue())).encode()).decode().replace('\n', '')
        buffer.close()
        record = self.env['invoice.report.file'].create({'file': out})
        return  '/web/binary/download_document?model=invoice.report.file&field=file&id=%s&filename=facturas_exportadas.csv' % (record.id)
