odoo.define('website_order_export_oin.website_order_export_oin', function (require) {
    "use strict";

    var sAnimations = require('website.content.snippets.animation');
    var ajax = require('web.ajax');

    sAnimations.registry.WebsiteOrderExport = sAnimations.Class.extend({
        selector: '.o_portal_wrap',
        read_events: {
            'click #quotation_export': '_onClickExportQuotations',
            'click #order_export': '_onClickExportOrders',
            'click #invoice_export': '_onClickExportInvoices',
            'click #order_repair_export': '_onClickExportOrders_Repair',
        },
        /**
         * @private
         */
        _onClickExportQuotations: function (ev){
            var checkedValues = false;
            checkedValues = $('input:Checkbox:checked').map(function() {
                return parseInt(this.id);
            }).get();
            return ajax.jsonRpc('/export/quotation/', 'call', {'quotation_ids': checkedValues.join(",")}).then(function(result) {
                window.open(result, '_blank');
            });
        },
        /**
         * @private
         */
        _onClickExportOrders: function (ev){
            var checkedValues = false;
            checkedValues = $('input:Checkbox:checked').map(function() {
                return parseInt(this.id);
            }).get();
            return ajax.jsonRpc('/export/order/', 'call', {'order_ids': checkedValues.join(",")}).then(function(result) {
                window.open(result, '_blank');
            });
        },
        /**
         * @private
         */
        _onClickExportOrders_Repair: function (ev){
            var checkedValues = false;
            checkedValues = $('input:Checkbox:checked').map(function() {
                return parseInt(this.id);
            }).get();
            return ajax.jsonRpc('/export/repair_order/', 'call', {'repair_order_ids': checkedValues.join(",")}).then(function(result) {
                window.open(result, '_blank');
            });
        },

        /**
         * @private
         */
        _onClickExportInvoices: function (ev){
            var checkedValues = false;
            checkedValues = $('input:Checkbox:checked').map(function() {
                return parseInt(this.id);
            }).get();
            return ajax.jsonRpc('/export/invoice/', 'call', {'invoice_ids': checkedValues.join(",")}).then(function(result) {
                window.open(result, '_blank');
            });
        },
    });
});
