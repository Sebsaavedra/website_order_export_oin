# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright 2018 Odoo IT now <http://www.odooitnow.com/>
# See LICENSE file for full copyright and licensing details.
#
##############################################################################
{
    'name': 'Website Order Export',
    'category': 'Website',
    'summary': 'Website: Export Quotations, Sale Orders and Invoices',

    'version': '12.0.1',
    'description': """
Website Order Export
====================
This module allows to export Quotations, Sale Orders and Invoices from the 
website My Account in .csv format.
        """,

    'author': 'Odoo IT now',
    'website': 'http://www.odooitnow.com/',
    'license': 'Other proprietary',

    'depends': [
        'website',
        'sale',
        'account'
        'repair.order'
        ],

    'data': [
        'security/ir.model.access.csv',
        'views/assets.xml',
        'views/templates.xml'
    ],
    'images': ['images/OdooITnow_screenshot.png'],

    'price': 15.0,
    'currency': 'EUR',

    'installable': True,
    'application': False
}
