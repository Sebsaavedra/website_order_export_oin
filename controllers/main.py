# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright 2018 Odoo IT now <http://www.odooitnow.com/>
# See LICENSE file for full copyright and licensing details.
#
##############################################################################

from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition
import base64


class OrderExport(http.Controller):

    @http.route('/web/binary/download_document', type='http', auth="public")
    @serialize_exception
    def download_document(self, model, field, id, filename=None, **kw):
        Model = request.env[model]
        rec = Model.sudo().browse([int(id)])
        res = rec.read([field])[0]
        filecontent = base64.b64decode(res.get(field) or '')
        if not filecontent:
            return request.not_found()
        else:
            if not filename:
                filename = '%s_%s' % (model.replace('.', '_'), id)
            return request.make_response(filecontent,
                           [('Content-Type', 'application/octet-stream'),
                            ('Content-Disposition', content_disposition(filename))])

    @http.route(['/export/order/',
                 '/export/quotation/'], type='json', auth="public", website=True)
    def export_orders(self, **post):
        order = False
        order_ids = []
        if post and post.get('quotation_ids', ''):
            order_ids = [int(quotation) for quotation in post.get('quotation_ids').split(',')]
        elif post and post.get('order_ids', ''):
            order = True
            order_ids = [int(order) for order in post.get('order_ids').split(',')]
        orders = request.env['sale.order'].browse(order_ids).exists()
        return orders.export_orders(order=order)

    @http.route('/export/invoice/', type='json', auth="public", website=True)
    def export_invoices(self, **post):
        invoice_ids = []
        if post and post.get('invoice_ids', ''):
            invoice_ids = [int(invoice) for invoice in post.get('invoice_ids').split(',')]
        invoices = request.env['account.invoice'].browse(invoice_ids).exists()
        return invoices.export_invoice()


####### SE AGREGA FUNCION PARA REPAIR ORDER ###############################

    @http.route('/export/repair_order/', type='json', auth="public", website=True)
    def export_repair_order(self, **post):
        repair_order_ids = []
        if post and post.get('repair_order_ids', ''):
            repair_order_ids = [int(repair_order) for repair_order in post.get('repair_order_ids').split(',')]
        repair_orders = request.env['repair.order'].browse(repair_order_ids).exists()
        return repair_orders.export_repair_order()
