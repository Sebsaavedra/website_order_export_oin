# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright 2018 Odoo IT now <http://www.odooitnow.com/>
# See LICENSE file for full copyright and licensing details.
#
##############################################################################

from odoo import fields, models, _


class InvoiceReportFile(models.TransientModel):
    _name = "invoice.report.file"

    file = fields.Binary('File')
